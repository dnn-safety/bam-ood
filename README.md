# BAM: Box Abstraction Monitors for Real-time OoD Detection in Object Detection

Implementation of BAM: Box Abstraction Monitors for Real-time OoD Detection in Object Detection
![bam architecure](BAM.png)

## Requirements

```
pip install -r requiremnets.txt
```

To install Detectron2, please follow [here](https://github.com/facebookresearch/detectron2).

## Models and Datasets Preparation

```
$ export DATASETS=<your_path_to_datasets_folder>
$ export MODELS=<your_path_to_checkpoints_folder>
# download datasets and checkpoints
$ bash scripts/download.sh
```

Please download BDD100K dataset manually to `$DATASET` dir from [BDD100K website](https://www.vis.xyz/bdd100k/).

To load custom dataset, the dataset folder should have the following structure:

```
 └── /path/to/dataset
     |
     ├── Data
     └── labels.json
```

Notice that the annotation file `labels.json` should be prepared in COCO format.
