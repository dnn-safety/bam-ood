cd ${DATASETS}
# download datasets
gdown --fuzzy https://drive.google.com/file/d/1K2o2ji0JwDghYqWzjbBNAscTpssDR3Nn/view?usp=sharing
gdown --fuzzy https://drive.google.com/file/d/17n-_-gVcOb6xSoQ3aiPgFeNZe3hK9_Fh/view?usp=sharing
gdown --fuzzy https://drive.google.com/file/d/1RJz1qo9eL8Nl3JFgHH5FrG-J_rSxG5J5/view?usp=sharing
gdown --fuzzy https://drive.google.com/file/d/1EIVv5E1Yt2aoMhnghFgD-BkJDoVXyl97/view?usp=sharing

# unpack
tar -xf ${DATASETS}/KITTI-ID.tar.gz -C ${DATASETS}
tar -xf ${DATASETS}/COCO-OoD.tar.gz -C ${DATASETS}
tar -xf ${DATASETS}/OpenImages-OoD.tar.gz -C ${DATASETS}
tar -xf ${DATASETS}/VOC-OoD.tar.gz -C ${DATASETS}

cd ${MODELS}
# download model weights
gdown --fuzzy https://drive.google.com/file/d/1LpR98Zz5ZzyV2EVConbqbobHBfS7Ei-G/view?usp=sharing
gdown --fuzzy https://drive.google.com/file/d/1LpuilEogbuuyVdI2h2pG68E-C-8qcheI/view?usp=sharing
gdown --fuzzy https://drive.google.com/file/d/1Yhplyf9OrBrDqeZbCI1-AnH1SE_hv2e9/view?usp=sharing
gdown --fuzzy https://drive.google.com/file/d/1Kl9QMGWfQn0CxtEVSC4nHnFDLJ9BlYZ5/view?usp=sharing
gdown --fuzzy https://drive.google.com/file/d/19MkRswl-NmAxHfGfFkAXuyccBqEou2df/view?usp=sharing
gdown --fuzzy https://drive.google.com/file/d/1efbsXY5tcePGPILCW-RGVhw7JmsQVIFa/view?usp=sharing
