from collections import ChainMap
from detectron2.data import MetadataCatalog
BDD_THING_CLASSES = ['pedestrian', 'rider', 'car', 'truck', 'bus', 'train', 'motorcycle', 'bicycle', 'traffic light', 'traffic sign']
BDD_THING_DATASET_ID_TO_CONTIGUOUS_ID = dict(
    ChainMap(*[{i + 1: i} for i in range(10)]))

KITTI_THING_CLASSES = ["Car", "Pedestrian", "Cyclist", "Van", "Truck", "Tram"]
KITTI_THING_DATASET_ID_TO_CONTIGUOUS_ID = dict(
ChainMap(*[{i + 1: i} for i in range(6)]))

COCO_THING_CLASSES = MetadataCatalog.get('coco_2017_train').thing_classes
COCO_THING_DATASET_ID_TO_CONTIGUOUS_ID = MetadataCatalog.get(
    'coco_2017_train').thing_dataset_id_to_contiguous_id

OPENIMAGES_THING_DATASET_ID_TO_CONTIGUOUS_ID = dict(
    ChainMap(*[{i + 1: i} for i in range(len(COCO_THING_CLASSES))]))

VOC_OOD_THING_CLASSES = ['airplane', 'bird', 'boat', 'bottle', 'cat', 'chair', 'couch', 'cow', 'dining table', 'dog', 'horse', 'potted plant', 'sheep', 'tv']
VOC_THING_DATASET_ID_TO_CONTIGUOUS_ID_in_domain = dict(
    ChainMap(*[{i + 1: i} for i in range(14)]))