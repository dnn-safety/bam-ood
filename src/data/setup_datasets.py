import os

# Detectron imports
from detectron2.data import MetadataCatalog
from detectron2.data.datasets import register_coco_instances

# Project imports
import data.metadata as metadata

def setup_kitti_dataset(dataset_dir):
    train_image_dir = os.path.join(dataset_dir, 'train_split/data')
    test_image_dir = os.path.join(dataset_dir, 'val_split/data')

    train_json_annotations = os.path.join(
        dataset_dir, 'train_split/labels.json')
    test_json_annotations = os.path.join(
        dataset_dir, 'val_split/labels.json')

    register_coco_instances(
        "kitti_custom_train",
        {},
        train_json_annotations,
        train_image_dir)
    MetadataCatalog.get(
        "kitti_custom_train").thing_classes = metadata.KITTI_THING_CLASSES
    MetadataCatalog.get(
        "kitti_custom_train").thing_dataset_id_to_contiguous_id = metadata.KITTI_THING_DATASET_ID_TO_CONTIGUOUS_ID

    register_coco_instances(
        "kitti_custom_val",
        {},
        test_json_annotations,
        test_image_dir)
    MetadataCatalog.get(
        "kitti_custom_val").thing_classes = metadata.KITTI_THING_CLASSES
    MetadataCatalog.get(
        "kitti_custom_val").thing_dataset_id_to_contiguous_id = metadata.KITTI_THING_DATASET_ID_TO_CONTIGUOUS_ID

def setup_voc_ood_dataset(dataset_dir):
    test_image_dir = os.path.join(dataset_dir, 'data')

    test_json_annotations = os.path.join(
        dataset_dir, 'labels.json')

    register_coco_instances(
        "voc_ood_val",
        {},
        test_json_annotations,
        test_image_dir)
    MetadataCatalog.get(
        "voc_ood_val").thing_classes = metadata.VOC_OOD_THING_CLASSES
    MetadataCatalog.get(
        "voc_ood_val").thing_dataset_id_to_contiguous_id = metadata.VOC_THING_DATASET_ID_TO_CONTIGUOUS_ID_in_domain

def setup_openim_ood_dataset(dataset_dir):
    test_image_dir = os.path.join(dataset_dir, 'images')

    test_json_annotations = os.path.join(
        dataset_dir, 'COCO-Format', 'val_coco_format.json')

    register_coco_instances(
        "openimages_ood_val",
        {},
        test_json_annotations,
        test_image_dir)
    MetadataCatalog.get(
        "openimages_ood_val").thing_classes = metadata.COCO_THING_CLASSES
    MetadataCatalog.get(
        "openimages_ood_val").thing_dataset_id_to_contiguous_id = metadata.OPENIMAGES_THING_DATASET_ID_TO_CONTIGUOUS_ID

def setup_bdd_dataset(dataset_dir):
    train_image_dir = os.path.join(dataset_dir, 'images/100k/train')
    test_image_dir = os.path.join(dataset_dir, 'images/100k/val')

    train_json_annotations = os.path.join(
        dataset_dir, 'train_bdd_converted.json')
    test_json_annotations = os.path.join(
        dataset_dir, 'val_bdd_converted.json')

    register_coco_instances(
        "bdd_custom_train",
        {},
        train_json_annotations,
        train_image_dir)
    MetadataCatalog.get(
        "bdd_custom_train").thing_classes = metadata.BDD_THING_CLASSES
    MetadataCatalog.get(
        "bdd_custom_train").thing_dataset_id_to_contiguous_id = metadata.BDD_THING_DATASET_ID_TO_CONTIGUOUS_ID

    register_coco_instances(
        "bdd_custom_val",
        {},
        test_json_annotations,
        test_image_dir)
    MetadataCatalog.get(
        "bdd_custom_val").thing_classes = metadata.BDD_THING_CLASSES
    MetadataCatalog.get(
        "bdd_custom_val").thing_dataset_id_to_contiguous_id = metadata.BDD_THING_DATASET_ID_TO_CONTIGUOUS_ID

def setup_coco_ood_bdd_dataset(dataset_dir):
    test_image_dir = os.path.join(dataset_dir, 'val2017')

    test_json_annotations = os.path.join(
        dataset_dir, 'instances_val2017_ood_wrt_bdd_rm_overlap.json')

    register_coco_instances(
        "coco_ood_val_bdd",
        {},
        test_json_annotations,
        test_image_dir)
    MetadataCatalog.get(
        "coco_ood_val_bdd").thing_classes = metadata.COCO_THING_CLASSES
    MetadataCatalog.get(
        "coco_ood_val_bdd").thing_dataset_id_to_contiguous_id = metadata.COCO_THING_DATASET_ID_TO_CONTIGUOUS_ID
