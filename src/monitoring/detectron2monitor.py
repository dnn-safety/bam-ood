import torch
from detectron2.utils.logger import setup_logger
setup_logger()

from detectron2.config import get_cfg
import detectron2.data.transforms as T
from detectron2.checkpoint import DetectionCheckpointer
from detectron2.modeling import build_model
from detectron2.data.detection_utils import read_image
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog


import numpy as np
import os
import pickle
import gradio as gr
import tqdm
import pandas as pd
from prettytable import PrettyTable
torch.manual_seed(0)
np.random.seed(0)

torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

from models.modeling.regnet import build_regnet_fpn_backbone
from data.setup_datasets import *

from utils.feature_extraction import feature_extraction
from utils.monitor_construction import features_clustering_by_k_start, monitor_construction_from_features
from utils.file_operations import load_pkl, save_pkl
from utils.evaluation import get_distance_dataset, compute_fpr95

class Detectron2Monitor():
    def __init__(self, id, backbone):
        self.id = id
        self.backbone = backbone
        self.eval_list = ['openimages_ood_val','voc_ood_val',f'{self.id}_custom_val','coco_ood_val_bdd']

    def _get_cfg(self):
        cfg = get_cfg()
        cfg.merge_from_file(f"models/configs/FX_{self.id}_{self.backbone}.yaml")
        cfg.MODEL.DEVICE='cuda'
        return cfg
    
    def _feature_extraction(self, cfg, dataset_name, batch_size=1):
        return feature_extraction(self.id, self.backbone, cfg, dataset_name, batch_size)
    
    def _construct(self, density):
        feats_dict = load_pkl(f"feats/{self.id}/{self.backbone}/{self.id}_custom_train.pkl")
        monitor_dict = {}
        for k, v in tqdm.tqdm(feats_dict.items(), desc="category loop", leave=False):
            # no clustering for categories with less than 1000 instances for KITTI, 20000 for BDD
            if len(v) < 1000:
                k_start = 2
            else:
                k_start = round(len(v)/density)
            clustering_results = features_clustering_by_k_start(v, k_start)
            monitor_dict[k] = monitor_construction_from_features(v, clustering_results)
        os.makedirs(f"monitors/{self.id}/{self.backbone}/", exist_ok=True)
        save_pkl(f"monitors/{self.id}/{self.backbone}/{density}.pkl", monitor_dict)

        counts_instances = dict()
        for k, v in feats_dict.items():
            counts_instances[k] = v.shape[0]
        counts_clusters = dict()
        for k, v in monitor_dict.items():
            counts_clusters[k] = len(v.good_ref)
        # Create a PrettyTable object
        table = PrettyTable()

        # Add columns to the table
        table.field_names = ["category", "#instances", "#clusters"]
        data = []
        for k in list(counts_instances.keys()):
            row = [k, counts_instances[k], counts_clusters[k]]
            table.add_row(row)
        table.sortby = "#instances"
        return table
    
    def _compute_id_threshold(self, density):
        monitors_path = f"monitors/{self.id}/{self.backbone}/{density}.pkl"
        monitors_dict = load_pkl(monitors_path)
        # ID evaluation
        feats_path = f"feats/{self.id}/{self.backbone}/{self.id}_custom_val.pkl"
        feats_id = load_pkl(feats_path)
        distances_dict = get_distance_dataset(monitors_dict, feats_id)
        distances_id = [distance for k, v in distances_dict.items() for distance in v]
        sorted_distances = sorted(distances_id)
        threshold_index = int(len(sorted_distances) * 0.95)
        threshold = sorted_distances[threshold_index]
        return threshold
    def _evaluate_ood(self, threshold, dataset_name, monitors_dict):
        return compute_fpr95(self.id, self.backbone, threshold, dataset_name, monitors_dict)
